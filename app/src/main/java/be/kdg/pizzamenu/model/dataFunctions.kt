package be.kdg.pizzamenu.model

fun getTestPizzas(): Array<Pizza> {
    return arrayOf(
        Pizza("Four Seasons", 20.0, 12.5),
        Pizza("Four Seasons", 25.0, 14.9),
        Pizza("Hawaii", 20.0, 13.5),
        Pizza("Pepperoni", 20.0, 11.5),
        Pizza("BBQ Chicken", 25.0, 14.5)
    )
}