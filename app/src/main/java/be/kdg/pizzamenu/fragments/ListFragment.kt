package be.kdg.pizzamenu.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import be.kdg.pizzamenu.R
import be.kdg.pizzamenu.adapters.OnPizzaSelectedListener
import be.kdg.pizzamenu.adapters.PizzaAdapter
import be.kdg.pizzamenu.model.getTestPizzas
import java.lang.Exception

class ListFragment : Fragment() {

    private lateinit var pizzas: RecyclerView
    private lateinit var pizzaListener: OnPizzaSelectedListener

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is OnPizzaSelectedListener) {
            pizzaListener = context
        } else {
            throw Exception("Context of ListFragment should "
                    + "always be a OnPizzaSelectedListener")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        initialiseViews(view)
        return view
    }

    private fun initialiseViews(view: View) {
        pizzas = view.findViewById(R.id.pizzas)

        pizzas.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = PizzaAdapter(
                getTestPizzas(),
                context,
                pizzaListener
            )
        }
    }

}
