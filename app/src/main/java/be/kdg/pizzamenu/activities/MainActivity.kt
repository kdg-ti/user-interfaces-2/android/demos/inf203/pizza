package be.kdg.pizzamenu.activities

import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import be.kdg.pizzamenu.R
import be.kdg.pizzamenu.adapters.OnPizzaSelectedListener
import be.kdg.pizzamenu.adapters.PizzaAdapter
import be.kdg.pizzamenu.fragments.DetailFragment
import be.kdg.pizzamenu.model.getTestPizzas

class MainActivity
    : AppCompatActivity(),
    OnPizzaSelectedListener {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onPizzaSelected(position: Int) {
        if (resources.configuration.orientation
            == Configuration.ORIENTATION_LANDSCAPE) {
            val detailFragment = supportFragmentManager
                .findFragmentById(R.id.detail_fragment)
                    as DetailFragment
            detailFragment.setSelectedPizza(
                getTestPizzas()[position]
            )
        } else {
            val intent = Intent(this, DetailActivity::class.java)
            val selectedPizza = getTestPizzas()[position]
            intent.putExtra("selectedPizza", selectedPizza)
            startActivity(intent)
        }
    }
}
