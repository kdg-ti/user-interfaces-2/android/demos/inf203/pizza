package be.kdg.pizzamenu.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import be.kdg.pizzamenu.R
import be.kdg.pizzamenu.fragments.DetailFragment
import be.kdg.pizzamenu.model.Pizza

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val pizza = intent.getSerializableExtra("selectedPizza")
                as Pizza

        // val fragment = findViewById(R.id.fragment_id)
        //  ... Helaas...

        val fragment = supportFragmentManager
            .findFragmentById(R.id.detail_fragment)
                as DetailFragment

        fragment.setSelectedPizza(pizza)
    }
}











