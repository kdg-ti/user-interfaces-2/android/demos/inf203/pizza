package be.kdg.pizzamenu.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.kdg.pizzamenu.R
import be.kdg.pizzamenu.model.Pizza

class PizzaAdapter(
    val pizzas: Array<Pizza>,
    val context: Context,
    val listener: OnPizzaSelectedListener
):
    RecyclerView.Adapter<PizzaViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PizzaViewHolder {
        val inflater = LayoutInflater.from(context)

        val view = inflater.inflate(R.layout.item_pizza, parent, false)

        return PizzaViewHolder(view)
    }

    override fun getItemCount(): Int = pizzas.size

    override fun onBindViewHolder(holder: PizzaViewHolder,
                                  position: Int) {
        holder.pizzaName.text = pizzas[position].name
        holder.price.text = pizzas[position].price.toString()
        holder.diameter.text = pizzas[position].diameter.toString()

        holder.itemView.setOnClickListener {
            listener.onPizzaSelected(position)
        }
    }
}
